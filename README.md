# Weekly Challenge - 1
* This is a classic classification problem in Machine Learning.
* We have three species of flowers in our dataset. 
* Each flower species has 50 datapoints.
* Each datapoint has 4 `features` associated with it namely `sepal length`,`sepalwidth`,`petal length` and `petal width`.
* Your task is to classify these three species using the 4 `features`.

# Dataset
* The CLASSIC `iris` dataset

# Guidelines
* You have to follow the __`SACRED`__ rule in __ML__ of `splitting dataset into training data and testing data.`
* The model that gives the highest accuracy from `70-30` or lower split will be the winner. 
* You can refer to the python notebook provided for guidance.
* you have to make __your own local__ repo for doing your project.
* For submissions comment inside `submissions` issue in __THIS__ repo. 